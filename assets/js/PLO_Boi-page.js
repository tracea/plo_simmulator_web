angular.module('simulate_plo', []);

angular.module('simulate_plo').controller('PageCtrl', [
  '$scope', '$http',
  function($scope, $http) {

    $scope.simulationError = false;


    $scope.runSimulation = function() {

      $scope.simulationError = false;
      if ($scope.busySimulating) {
        return;
      }

      $scope.busySimulating = true;

      io.socket.get('/PLO/simulate?cards='+$scope.Hand1 + " "
                    + $scope.Hand2, function whenServerResponds(data, JWR) {
                      if (JWR.statusCode >= 400) {
                        $scope.simulationError = true;
                        return;
                      }
                      $scope.busySimulating = false;

                      if (data["player0"] == "Invalid") {
                        $scope.simulationError = true;
                      } else {
                        $scope.Hand1 = Math.floor(parseInt(data["player0"]) / 100)+"% Win Rate";
                        $scope.Hand2 = Math.floor(parseInt(data["player1"]) / 100)+"% Win Rate";
                      }
                      $scope.$apply();
                    });
    };
  }
]);
