/**
 * PLOController
 *
 * @description :: Server-side logic for managing PLOES
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  simulate: function(req, res) {
    var cards = req.param('cards');
    var shell = require('shelljs');

    if( /[^a-zA-Z0-9" "]/.test( cards ) ) {
      res.send({"player0":"Invalid"});
    }else {
      shell.exec("./PLO_Code/PLO_SIM " + cards, function(code, stdout, stderr) {
        if (code) {
          // should have err.code here?
        }
        var winners = stdout.split(/\n/);
        var to_return = {}
        for (i = 0; i < winners.length - 1; i++) {
          to_return["player"+i] = winners[i];
        }
        res.send(to_return);
      });
    }
  }
};
