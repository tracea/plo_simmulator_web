#include "cards.h"
#include <vector>


#ifndef PLO_GAME
#define PLO_GAME

class PLO {
 public:
  PLO(std::vector<std::vector<int>> hands);
  int simGame();

 private:
  std::vector<std::vector<int>> hands;
  Deck deck;
};

#endif
