#include <vector>
#include <iostream>
#include "PLO.h"
#include <tuple>

PLO::PLO(std::vector<std::vector<int>> hands) {
  this->hands = hands;
  this->deck = Deck(hands);
};

int PLO::simGame() {
  this->deck.shuffle();
  Rank min = {0,{0}};
  std::pair<Rank,int> winner = std::make_pair(min, -1);
  for (int i = 0; i < hands.size(); ++i) {
    Rank max = {0,{0}};
    std::vector<std::vector<int>> possibleHand;
    std::vector<std::vector<int>> possibleBoard;
    for (int j = 0; j < hands[i].size() - 1; ++j) {
      for (int k = 1 + j; k < hands[i].size(); ++k) {
        std::vector<int> tmp = {j,k};
        possibleHand.emplace_back(tmp);
      }
    }

    for (int j = 0; j < 5 - 2; ++j) {
      for (int k = 1 + j; k < 5 - 1; ++k) {
        for (int l = k + 1; l < 5; ++l) {
          std::vector<int> tmp = {j,k,l};
          possibleBoard.emplace_back(tmp);
        }
      }
    }
    for (int j = 0; j < possibleHand.size(); ++j) {
      for (int k = 0; k < possibleBoard.size(); ++k) {
        
        auto h = Hand({hands[i][possibleHand[j][0]],
                      hands[i][possibleHand[j][1]],
                      this->deck.get(possibleBoard[k][0]),
                      this->deck.get(possibleBoard[k][1]),
                      this->deck.get(possibleBoard[k][2])});
        max = max < h.getRank() ? h.getRank() : max;
    
      }
    }
    
    winner = winner.first < max ? std::make_pair(max,i) :
      winner.first == max ? std::make_pair(max,-1) : winner;
  }
  return winner.second;
};


