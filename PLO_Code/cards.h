#include <vector>


#ifndef CARDS_PLO
#define CARDS_PLO

struct Rank{
  int rank;
  std::vector<int> kickers;
};

bool operator <(const Rank& x, const Rank& y);

bool operator ==(const Rank& x, const Rank& y);



class Hand {
  // Hand class just a wrapper for a vector of intergers.
public:
  Hand(std::vector<int> h1);
  void addCard(int c);
  std::vector<int>::iterator getStart();
  std::vector<int>::iterator getEnd();
  bool operator<(const Hand& other) const;
  bool operator==(const Hand& other) const;
  bool operator>(const Hand& other) const;
  int compare(const Hand &other);
  Rank getRank();
private:
  Rank rank;
  std::vector<int> hand;
};

class Deck {
public:
  Deck(std::vector<std::vector<int>> hands);
Deck();
  void shuffle();
  std::vector<int>::iterator getStart();
  std::vector<int>::iterator getEnd();
int get(int i);
  
private:
  std::vector<int> deck;
  int index;
};

#endif
