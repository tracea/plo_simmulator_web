#include <iostream>
#include <vector>
#include "PLO.h"
#include "cards.h"
#include <sstream>


int main(int argc, char *argv[]) {
  //std::vector<int> h = {1,2,3,4};
  //std::vector<int> h2 = {14,15,16,17};
  std::vector<std::vector<int>> hands;
  int cur_hand = -1;
  for (int i = 0; i < argc - 1; ++i) {
    
    std::istringstream ss(argv[i+1]);
    if (!(i % 4)) {
      std::vector<int> tmp;
      hands.emplace_back(tmp);
      ++cur_hand;
    }
    int x;
    ss >> x;
    hands[cur_hand].emplace_back(x);
    
  }
  
  auto p = PLO(hands);
  std::vector<int> games;
  for (int i = 0; i < hands.size(); ++i) {
    games.emplace_back(0);
  }
  
  for (int i = 0; i < 10000; ++i){
    int winner = p.simGame();
    if (winner != -1) {
      ++games[winner];
    }
    
  }
  for (int i = 0; i < games.size(); ++i) {
    std::cout << games[i] << std::endl;
  }
}
