#include <iostream>
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock
#include <vector>
#include <deque>
#include <unordered_map>
#include "cards.h"

std::random_device GLOBAL_rd;
std::mt19937 GLOBAL_rng(GLOBAL_rd());
/* 
Cards are kept as integers because I'm looking for speed.
9 = jack, 10 = queen, 11 = king, 12 = ace
*/

int cardValue(int i) { return i % 13; }

struct {
  bool operator()(int a, int b) const {
    return a%13 > b%13;
  }
} cardCompare;

struct {
  bool operator()(std::pair<int,int> a,
                  std::pair<int,int> b) const {
    return a.second != b.second ? a.second > b.second :
      a.first > b.first;
  }
} matchCompare;


Hand::Hand(std::vector<int> h1) {
  hand = h1;
  std::sort(hand.begin(),hand.end(),cardCompare);

  int cur_suit, cur_rank, final_rank;
  int fourk = 0, threek = 0, twok = 0;
  bool straight = false, awrap = false, flush = false;
  
  std::vector<std::pair<int,int>> matches;
  std::unordered_map<int,int> rank_counts, suit_counts,
    straight_counts;
  std::vector<int> kickers;
  
  (hand[0] % 13 == 12 && hand[1] % 13 == 3) ?
    ++straight_counts[4], awrap = true :
    ++straight_counts[hand[0] % 13];
    
  ++rank_counts[hand[0] % 13];
  ++suit_counts[hand[0] / 13];
  for (int i = 1; i < hand.size(); ++i) {
    cur_rank = hand[i] % 13;
    
    ++straight_counts[cur_rank + i];
    ++rank_counts[cur_rank];
    ++suit_counts[(hand[i] / 13)];
  }
    
  straight = straight_counts.size() == 1;
  flush = suit_counts.size() == 1;
    
  if (rank_counts.size() < 5) {  // Must have pairs
    
    for (auto pairs : rank_counts) {
      matches.emplace_back(pairs);
      //std::cout << pairs.second << std::endl;
      pairs.second == 2 ? ++twok :
        pairs.second == 3 ? ++threek :
        pairs.second == 4 ? ++fourk : 1;
    }
    
    std::sort(matches.begin(),matches.end(),matchCompare);
    for (auto match : matches) {
      kickers.emplace_back(match.first);
    }
    
    rank = {(fourk == 1) ? 7 : (twok == 2) ? 2 :
            ((threek == 1) && (twok == 1)) ? 6 :
            (threek == 1) ? 3 : 1, kickers};
  } else {
    kickers.resize(hand.size());
    std::transform(hand.begin(), hand.end(),
                   kickers.begin(), cardValue); 
    if(straight & flush) {
      rank = {8 , {awrap ? 3 : hand[0] % 13}};
    } else if (flush) {
      rank = {5 , kickers};
    } else if (straight) {
      rank = {4 , {awrap ? 3 : hand[0] % 13}};
    } else {
      rank = {0, kickers};
    }
  }   
}

void Hand::addCard(int c){
  this->hand.push_back(c);
}

std::vector<int>::iterator Hand::getStart() {
  return hand.begin();
}

std::vector<int>::iterator Hand::getEnd() {
  return hand.end();
}

bool Hand::operator<(const Hand& other) const {
  return (this->rank.rank != other.rank.rank) ?
    this->rank.rank < other.rank.rank :
    this->rank.kickers < other.rank.kickers;
}

bool operator <(const Rank& x, const Rank& y){
  return (x.rank != y.rank) ?
    x.rank < y.rank :
    x.kickers < y.kickers;
}

bool Hand::operator>(const Hand& other) const{
  return other < *this;
}

bool Hand::operator==(const Hand& other) const {
  return  (this->rank.rank != other.rank.rank) ?
    false : this->rank.kickers == other.rank.kickers;
}

bool operator ==(const Rank& x, const Rank& y){
  return  (x.rank != y.rank) ? false :
    x.kickers == y.kickers;
}

int Hand::compare(const Hand &other) {
  
  /*
    Returns 1 if this beats other,
    Returns 0 if this is equal to other
    Returns -1 if this is loses to other
   */

  return 1;
}

Rank Hand::getRank() {
  return rank;
}

Deck::Deck(std::vector<std::vector <int>> hands){
  std::deque<bool> cards(52,true);
    
  for (int i = 0; i < hands.size(); ++i) {
    for (auto it = hands[i].begin();
         it != hands[i].end(); ++it) {
      cards[*it] = false;
    }
  }
    
  for (int i = 0; i < 52; ++i) {
    if(cards[i]){
      deck.emplace_back(i);
    }
  }
  index = 0;
};

Deck::Deck(){};

void Deck::shuffle(){
  std::shuffle(deck.begin(),deck.end(),GLOBAL_rng);
};

int Deck::get(int i){
  return this->deck[i];
}

std::vector<int>::iterator Deck::getStart() {
  return deck.begin();
}

std::vector<int>::iterator Deck::getEnd() {
  return deck.end();
}



